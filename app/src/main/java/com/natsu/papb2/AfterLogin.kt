package com.natsu.papb2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class AfterLogin : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_after_login)
        val Hasil = findViewById<TextView>(R.id.Hasil)

        val username = intent.getStringExtra("EXTRA_USERNAME")
        val password = intent.getStringExtra("EXTRA_PASSWORD")

        val hasilData = "Username : $username \n" +
                "Password : $password \n"
        Hasil.text = hasilData

    }
}