package com.natsu.papb2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnSubmit = findViewById<Button>(R.id.btnSubmit)
        val iniUsername = findViewById<EditText>(R.id.iniUsername)
        val iniPassword = findViewById<EditText>(R.id.iniPassword)

        btnSubmit.setOnClickListener{
            val username = iniUsername.text.toString()
            val password = iniPassword.text.toString()
            Intent(this, AfterLogin::class.java).also {
                it.putExtra("EXTRA_USERNAME", username)
                it.putExtra("EXTRA_PASSWORD", password)
                startActivity(it)
            }
        }

    }
}